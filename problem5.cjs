
// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory = require('./inventory.cjs')
const get_all_car_years = require('./problem4.cjs')
const get_all_years = require('./problem4.cjs')

function count_cars_2000(){
    const carYears = get_all_car_years()
    const older_cars = [];


    for (let index = 0; index<carYears.length; index++){
        if (carYears[index]<2000) {
            older_cars.push(carYears[index])
        }
    }


    console.log(`Number of cars older than 2000: ${older_cars.length}`);
    return older_cars;
}


module.exports = count_cars_2000