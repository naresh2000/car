const get_last_car = require('../problem2.cjs')

const last_car = get_last_car();

console.log(`Last car is a ${last_car.car_make} ${last_car.car_model}`)