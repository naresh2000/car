const find_car_id = require('../problem1.cjs')
const inventory = require('../inventory.cjs');


const car_id = 33;
const car = find_car_id(inventory,car_id);

if (car) {
 console.log(`Car ${car_id} is a ${car.car_year} ${car.car_make} ${car.car_model}`)   
}
