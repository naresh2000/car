// // ==== Problem #2 ====
// // The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
// "Last car is a *car make goes here* *car model goes here*"

const inventory = require('./inventory.cjs')



function get_last_car(){
     if (inventory.length > 0){
        const last_car = inventory[inventory.length -1];
        return last_car
     }
}


module.exports = get_last_car
