// ==== Problem #3 ====
// The marketing team wants the car models listed 
// alphabetically on the website. Execute a function to 
// Sort all the car model names into alphabetical order 
// and log the results in the console as it was
//  returned.

const inventory = require('./inventory.cjs')


function sort_car_models(){
    const car_models = [];

    for (let index = 0;index<inventory.length;index++){
        car_models.push(inventory[index].car_model)
    }
    car_models.sort();
    return car_models
}

module.exports = sort_car_models;